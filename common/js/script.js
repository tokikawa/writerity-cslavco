$('document').ready(function () {
	$('a').on('click', function (e) {
		if ( e.currentTarget.id != 'mailto-btn' ) {
			e.preventDefault();
		}
	});
	
	$('.navigation-content ul li a').on('click', function () {
		var id = $(this).attr('id');
		$('html, body').animate({
			scrollTop: $('#' + id + '.section').offset().top
		}, 1000);
	});
});

window.onscroll = function () {
	scrollFunction();
}



function scrollFunction() {
	if (  $('body, html').scrollTop() > 20 ) {
		$('#goto-top').fadeIn('slow');
	} else {
		$('#goto-top').fadeOut('slow');
	}
}

function gotoTop() {
	$('html, body').animate({ scrollTop: 0 }, 1500);
}


function myFunction() {
    var x = document.getElementById("nav-menu");
    if (x.className === "myTopnav") {
        x.className += " responsive";
    } else {
        x.className = "myTopnav";
    }

}


function myFunction() {
    var x = document.getElementById("nav-menu");
    var y = document.getElementById("full-container");
    if (x.className === "myTopnav" && y.className === "container") {
        x.className += " responsive";
        y.className += " full-page-container"
    } else {
        x.className = "myTopnav";
        y.className = "container";
    }
     
}


function initMap() {
	var latlon = {lat: 33.571381, lng: 133.536008};
        var map = new google.maps.Map(document.getElementById('section2-map'), {zoom: 15, center: latlon});
        var marker = new google.maps.Marker({position: latlon, map: map});
}
