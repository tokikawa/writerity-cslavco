
(document).ready(function(){
    $("#nav-mobile").html($("#nav-main").html());
    $("#nav-trigger span").click(function(){
        if ($("nav#nav-mobile ul").hasClass("expanded")) {
            $("nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
            $(this).removeClass("open");
        } else {
            $("nav#nav-mobile ul").addClass("expanded").slideDown(250);
            $(this).addClass("open");
        }
    });
});

function callNumber(){
    if (screen.width <= 730) {
    	document.getElementById("call_number").setAttribute("href","tel:088-821-6680");
    } else {
    	document.getElementById("call_number").setAttribute("href","#");
    }
}